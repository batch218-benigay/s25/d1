// [SECTION] JSON Objects
/*
	JSON stands for JavaScript Object Notation
	-commonly used in:
		~reading data from a web server
		~display the data in a web page

	Features:
		-lightweight data-interchange format
		- easy to read and write
		-for machines to parse and generate
*/

// JSON Objects
/*		-also use the "key/value pairs" just like the object properties in JavaScript
		"Key/property" names requires to be enclosed with double quotes (" ")

		Syntax:
		{
			"propertyA" : "valueA",
			"propertyB" : "value"
		}
*/

//Example of JSON object
/*
{
	"city" : "Quezon City",
	"province" : "Metro Manila",
	"country" : "Philippines" 
}
*/

//Arrays of JSON Objects

/*
"cities" : [
{
	"city" : "Quezon City",
	"province" : "Metro Manila",
	"country" : "Philippines" 
},
{
	"city" : "Manila City",
	"province" : "Metro Manila",
	"country" : "Philippines" 
},
{
	"city" : "Quezon City",
	"province" : "Metro Manila",
	"country" : "Philippines" 
}
]
*/
console.log("---------------------");
//[SECTION] Converting JavaScript Objects to JSON Object
// JavaScript Array of Objects
let batchesArr = [
	{
		batchName: "Batch 218",
		schedule: "Part Time",
	},
	{
		batchName: "Batch 219",
		schedule: "Full Time",
	}
]

console.log(batchesArr);

// The "stringify" method is used to convert JavaScript Objects into a string

console.log("Result of stringify method: ");
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify({
	name : "John",
	age : 31,
	address : {
		city: "Manila",
		country : "Philippines"
	}
})

console.log(data);

/*let firstName = prompt("Enter your first name:");
let lastName = prompt("Enter your last name:");
let email = prompt("Enter your email:");
let password = prompt("Enter your password:");

let otherData = JSON.stringify(
	{
	firstName : firstName,
	lastName : lastName,
	email : email,
	password : password
	}
)

console.log(otherData);*/

//[SECTION] Converting stringified JSON into JavaScript Object

let batchesJSON = `[
	{
		"batchName" : "Batch 218",
		"schedule" : "Part Time"
	},
	{
		"batchName" : "Batch 219",
		"schedule" : "Full Time"
	}
]`

console.log("batchesJSON content:");
console.log(batchesJSON);

// JSON.parse method converts JSON objects into JavaScript Object
console.log("Result from parse method:");
// console.log(JSON.parse(batchesJSON));

let parseBatches = JSON.parse(batchesJSON);
console.log(parseBatches);
console.log(parseBatches[0].batchName);

let stringifiedObject = `{
	"name" : "John",
	"age" : 31,
	"address" : {
		"city" : "Manila",
		"country" : "Philippines"
	}
}`

console.log(stringifiedObject);
console.log(JSON.parse(stringifiedObject))